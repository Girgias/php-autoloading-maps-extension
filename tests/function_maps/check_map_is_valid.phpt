--TEST--
Check that assigned function map works
--EXTENSIONS--
autoloading_maps
--FILE--
<?php

function test_map(array $map) {
	try {
		autoload_register_function_map($map);
	} catch (\Throwable $e) {
		echo $e::class, ': ', $e->getMessage(), PHP_EOL;
	}
}

$pathTo = __DIR__ . DIRECTORY_SEPARATOR . 'functions' . DIRECTORY_SEPARATOR;
$map = [
    'foo' => $pathTo . "foo.inc",
    'not a symbol name' => $pathTo . "bar.inc",
];
test_map($map);

$map = [
    'foo' => $pathTo . "foo.inc",
    0 => $pathTo . "bar.inc",
];
test_map($map);

$map = [
    'foo' => 0,
];
test_map($map);

$map = [
    'foo' => "",
];
test_map($map);

$map = [
    'foo' => "Path\0hasNull",
];
test_map($map);

?>
--EXPECT--
ValueError: autoload_register_function_map(): Argument #1 ($functions) key "not a symbol name" is not a valid function name
ValueError: autoload_register_function_map(): Argument #1 ($functions) must only have string keys
TypeError: autoload_register_function_map(): Argument #1 ($functions) path for function "foo" must be of type string
ValueError: autoload_register_function_map(): Argument #1 ($functions) path for function "foo" must not be empty
ValueError: autoload_register_function_map(): Argument #1 ($functions) path for function "foo" must not contain null bytes
