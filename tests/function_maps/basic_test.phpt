--TEST--
Check that assigned class map works
--EXTENSIONS--
autoloading_maps
--FILE--
<?php
$pathTo = __DIR__ . DIRECTORY_SEPARATOR . 'functions' . DIRECTORY_SEPARATOR;
$classes = [
    'foo' => $pathTo . "foo.inc",
    'bar' => $pathTo . "bar.inc",
];

autoload_register_function_map($classes);

foo();
bar();

?>
--EXPECT--
foo function
bar function
