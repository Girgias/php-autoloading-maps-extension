dnl config.m4 for extension autoloading_maps

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary.

dnl If your extension references something external, use 'with':

dnl PHP_ARG_WITH([autoloading_maps],
dnl   [for autoloading_maps support],
dnl   [AS_HELP_STRING([--with-autoloading_maps],
dnl     [Include autoloading_maps support])])

dnl Otherwise use 'enable':

PHP_ARG_ENABLE([autoloading_maps],
  [whether to enable autoloading_maps support],
  [AS_HELP_STRING([--enable-autoloading_maps],
    [Enable autoloading_maps support])],
  [no])

if test "$PHP_AUTOLOADING_MAPS" != "no"; then
  dnl In case of no dependencies
  AC_DEFINE(HAVE_AUTOLOADING_MAPS, 1, [ Have autoloading_maps support ])

  STD_CFLAGS="-std=c99"

  dnl Drop -Wpedantic due to "Zend/zend.h:117:3: error: ISO C99 doesn’t support unnamed structs/unions"
  MAINTAINER_CFLAGS="-Wall -Wextra"
  AX_CHECK_COMPILE_FLAG(-Wfatal-errors,         MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wfatal-errors")
  AX_CHECK_COMPILE_FLAG(-Werror,                MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Werror")
  AX_CHECK_COMPILE_FLAG(-Wno-unused-parameter,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wno-unused-parameter")
  dnl Fast ZPP API uses shadow variables
  dnl AX_CHECK_COMPILE_FLAG(-Wshadow,               MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wshadow")
  AX_CHECK_COMPILE_FLAG(-Wdouble-promotion,     MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wdouble-promotion")
  AX_CHECK_COMPILE_FLAG(-Wformat=2,             MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wformat=2")
  AX_CHECK_COMPILE_FLAG(-Wformat-truncation,    MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wformat-truncation")
  AX_CHECK_COMPILE_FLAG(-Wduplicated-cond,      MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wduplicated-cond")
  AX_CHECK_COMPILE_FLAG(-Wduplicated-branches,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wduplicated-branches")
  AX_CHECK_COMPILE_FLAG(-Wlogical-op,           MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wlogical-op")
  AX_CHECK_COMPILE_FLAG(-Wrestrict,             MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wrestrict")
  AX_CHECK_COMPILE_FLAG(-Wnull-dereference,     MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wnull-dereference")
  AX_CHECK_COMPILE_FLAG(-Wlogical-not-parentheses,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wlogical-not-parentheses")

  PHP_AUTOLOADING_MAPS_CFLAGS="$STD_CFLAGS $MAINTAINER_CFLAGS"

  PHP_NEW_EXTENSION(autoloading_maps, autoloading_maps.c, $ext_shared, , $PHP_AUTOLOADING_MAPS_CFLAGS)
fi
