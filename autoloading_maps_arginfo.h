/* This is a generated file, edit the .stub.php file instead.
 * Stub hash: 09fbc1b510c56e2a46b75e533a831b88b913971e */

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_test1, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_autoload_register_class_map, 0, 1, IS_VOID, 0)
	ZEND_ARG_TYPE_INFO(0, classes, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_autoload_register_function_map, 0, 1, IS_VOID, 0)
	ZEND_ARG_TYPE_INFO(0, functions, IS_ARRAY, 0)
ZEND_END_ARG_INFO()


ZEND_FUNCTION(test1);
ZEND_FUNCTION(autoload_register_class_map);
ZEND_FUNCTION(autoload_register_function_map);


static const zend_function_entry ext_functions[] = {
	ZEND_FE(test1, arginfo_test1)
	ZEND_FE(autoload_register_class_map, arginfo_autoload_register_class_map)
	ZEND_FE(autoload_register_function_map, arginfo_autoload_register_function_map)
	ZEND_FE_END
};
