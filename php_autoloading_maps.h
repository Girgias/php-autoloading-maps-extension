/* autoloading_maps extension for PHP */

#ifndef PHP_AUTOLOADING_MAPS_H
# define PHP_AUTOLOADING_MAPS_H

#include "zend_modules.h"
extern zend_module_entry autoloading_maps_module_entry;
# define phpext_autoloading_maps_ptr &autoloading_maps_module_entry

# define PHP_AUTOLOADING_MAPS_VERSION "0.1.0"

# if defined(ZTS) && defined(COMPILE_DL_AUTOLOADING_MAPS)
ZEND_TSRMLS_CACHE_EXTERN()
# endif

#endif	/* PHP_AUTOLOADING_MAPS_H */
