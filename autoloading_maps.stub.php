<?php

/**
 * @generate-class-entries
 * @undocumentable
 */

function autoload_register_class_map(array $classes): void {}

function autoload_register_function_map(array $functions): void {}
