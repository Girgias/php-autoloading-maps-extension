/* autoloading_maps extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h" /* php_info_print_table_start */
#include "php_autoloading_maps.h"
#include "zend_hash.h"
#include "zend_autoload.h"
#include "zend_exceptions.h"
#include "zend_API.h"
#include "autoloading_maps_arginfo.h"

ZEND_TLS HashTable *autoloader_class_map;
ZEND_TLS HashTable *autoloader_function_map;

void zend_autoload_maps_shutdown(void)
{
	if (autoloader_class_map) {
		zend_array_release(autoloader_class_map);
		autoloader_class_map = NULL;
	}
	if (autoloader_function_map) {
		zend_array_release(autoloader_function_map);
		autoloader_function_map = NULL;
	}
}

#define ZEND_FAKE_OP_ARRAY ((zend_op_array*)(intptr_t)-1)

static zend_never_inline zend_op_array* zend_autoload_execute_file(zend_string *file_path)
{
	zend_op_array *new_op_array = NULL;
	zend_file_handle file_handle;
	zend_string *resolved_path;

	ZEND_ASSERT(!zend_str_has_nul_byte(file_path));

	resolved_path = zend_resolve_path(file_path);
	if (EXPECTED(resolved_path)) {
		/* If file has already been required once */
		if (zend_hash_exists(&EG(included_files), resolved_path)) {
			zend_string_release_ex(resolved_path, false);
			return ZEND_FAKE_OP_ARRAY;
		}
	} else if (UNEXPECTED(EG(exception))) {
		return NULL;
	} else {
		resolved_path = zend_string_copy(file_path);
	}

	zend_stream_init_filename_ex(&file_handle, resolved_path);
	if (SUCCESS == zend_stream_open(&file_handle)) {
		if (!file_handle.opened_path) {
			file_handle.opened_path = zend_string_copy(resolved_path);
		}

		if (zend_hash_add_empty_element(&EG(included_files), file_handle.opened_path)) {
			new_op_array = zend_compile_file(&file_handle, ZEND_REQUIRE);
		} else {
			new_op_array = ZEND_FAKE_OP_ARRAY;
		}
	} else if (!EG(exception)) {
		zend_message_dispatcher(ZMSG_FAILED_REQUIRE_FOPEN, file_path);
	}
	zend_destroy_file_handle(&file_handle);
	zend_string_release_ex(resolved_path, false);

	return new_op_array;
}

ZEND_API zend_class_entry *zend_perform_class_map_autoload(zend_string *class_name, zend_string *lc_name)
{
	if (!autoloader_class_map) {
		return zend_perform_class_autoload(class_name, lc_name);
	}

	zval *class_file_path = zend_hash_find_deref(autoloader_class_map, class_name);
	/* An entry has been found in the class map */
	if (class_file_path) {
		ZEND_ASSERT(Z_TYPE_P(class_file_path) == IS_STRING);

		zend_op_array *op_array = zend_autoload_execute_file(Z_STR_P(class_file_path));
		if (op_array != NULL && op_array != ZEND_FAKE_OP_ARRAY) {
			destroy_op_array(op_array);
			efree_size(op_array, sizeof(zend_op_array));
		}

		if (EG(exception)) {
			return NULL;
		}

		zend_class_entry *ce = zend_hash_find_ptr(EG(class_table), lc_name);
		if (EXPECTED(ce)) {
			return ce;
		}

		/* if the item is in the classmap it must be valid after including the file */
		zend_throw_error(NULL, "Error during autoloading from classmap. Entry \"%s\" failed to load the class from \"%s\" (Class undefined after file included)",
			ZSTR_VAL(class_name), Z_STRVAL_P(class_file_path));
		return NULL;
	}


	return zend_perform_class_autoload(class_name, lc_name);
}

ZEND_API zend_function *zend_perform_function_map_autoload(zend_string *function_name, zend_string *lc_name)
{
	if (!autoloader_function_map) {
		return zend_perform_function_autoload(function_name, lc_name);
	}

	zval *file_path = zend_hash_find_deref(autoloader_function_map, function_name);
	/* An entry has been found in the class map */
	if (file_path) {
		ZEND_ASSERT(Z_TYPE_P(file_path) == IS_STRING);

		zend_op_array *op_array = zend_autoload_execute_file(Z_STR_P(file_path));
		if (op_array != NULL && op_array != ZEND_FAKE_OP_ARRAY) {
			destroy_op_array(op_array);
			efree_size(op_array, sizeof(zend_op_array));
		}

		if (EG(exception)) {
			return NULL;
		}

		zend_function *func = zend_hash_find_ptr(EG(function_table), lc_name);
		if (EXPECTED(func)) {
			return func;
		}

		/* if the item is in the classmap it must be valid after including the file */
		zend_throw_error(NULL, "Error during autoloading from function map. Entry \"%s\" failed to load the function from \"%s\" (function undefined after file included)",
			ZSTR_VAL(function_name), Z_STRVAL_P(file_path));
		return NULL;
	}

	return zend_perform_function_autoload(function_name, lc_name);
}

static bool zend_check_map_is_valid(/* const */ HashTable *map, const char* symbol_type) {
	zend_string *name;
	zval *zval_path;
	ZEND_HASH_FOREACH_STR_KEY_VAL(map, name, zval_path) {
		if (!name) {
			  zend_argument_value_error(1, "must only have string keys");
			  return false;
		}
		if (!zend_is_valid_symbol_name(name)) {
			  zend_argument_value_error(1, "key \"%s\" is not a valid %s name", ZSTR_VAL(name), symbol_type);
			  return false;
		}
		if (Z_TYPE_P(zval_path) != IS_STRING) {
			  zend_argument_type_error(1, "path for %s \"%s\" must be of type string", symbol_type, ZSTR_VAL(name));
			  return false;
		}
		if (Z_STRLEN_P(zval_path) == 0) {
			zend_argument_value_error(1, "path for %s \"%s\" must not be empty", symbol_type, ZSTR_VAL(name));
			  return false;
		}
		if (zend_str_has_nul_byte(Z_STR_P(zval_path))) {
			zend_argument_value_error(1, "path for %s \"%s\" must not contain null bytes", symbol_type, ZSTR_VAL(name));
			  return false;
		}
	} ZEND_HASH_FOREACH_END();

	return true;
}

PHP_FUNCTION(autoload_register_class_map)
{
	HashTable *class_map;

	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY_HT(class_map)
	ZEND_PARSE_PARAMETERS_END();

	if (UNEXPECTED(!zend_check_map_is_valid(class_map, "class"))) {
		RETURN_THROWS();
	}

	autoloader_class_map = zend_array_dup(class_map);
}

PHP_FUNCTION(autoload_register_function_map)
{
	HashTable *function_map;

	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY_HT(function_map)
	ZEND_PARSE_PARAMETERS_END();

	if (UNEXPECTED(!zend_check_map_is_valid(function_map, "function"))) {
		RETURN_THROWS();
	}

	autoloader_function_map = zend_array_dup(function_map);
}

PHP_MINIT_FUNCTION(autoloading_maps)
{
	zend_autoload_class = zend_perform_class_map_autoload;
	zend_autoload_function = zend_perform_function_map_autoload;
	return SUCCESS;
}

PHP_RINIT_FUNCTION(autoloading_maps)
{
#if defined(ZTS) && defined(COMPILE_DL_AUTOLOADING_MAPS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	autoloader_class_map = NULL;
	autoloader_function_map = NULL;
	return SUCCESS;
}
/* }}} */


PHP_RSHUTDOWN_FUNCTION(autoloading_maps) /* {{{ */
{
	zend_autoload_maps_shutdown();
	return SUCCESS;
} /* }}} */

/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(autoloading_maps)
{
	php_info_print_table_start();
	php_info_print_table_row(2, "autoloading_maps support", "enabled");
	php_info_print_table_end();
}
/* }}} */

/* {{{ autoloading_maps_module_entry */
zend_module_entry autoloading_maps_module_entry = {
	STANDARD_MODULE_HEADER,
	"autoloading_maps",					/* Extension name */
	ext_functions,					/* zend_function_entry */
	PHP_MINIT(autoloading_maps), /* PHP_MINIT - Module initialization */
	NULL,							/* PHP_MSHUTDOWN - Module shutdown */
	PHP_RINIT(autoloading_maps),			/* PHP_RINIT - Request initialization */
	PHP_RSHUTDOWN(autoloading_maps),							/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(autoloading_maps),			/* PHP_MINFO - Module info */
	PHP_AUTOLOADING_MAPS_VERSION,		/* Version */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_AUTOLOADING_MAPS
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(autoloading_maps)
#endif
