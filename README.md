# Map autoloading extension

This a companion extension of the [Core Autoloading RFC](https://wiki.php.net/rfc/core-autoloading),
which implements support for symbol maps (class map and function map).

## Install

Check out the git branch: https://github.com/Girgias/php-src/tree/zend_autoloader

And build PHP with whatever usual configure flags with an added ``--prefix`` option to not clash with the standard PHP installation
```shell
./buildconf
./configure -C CC=gcc --prefix /home/path/to/custom-php/core-autoloading-php
make -jn
make install
```

Then check-out the extension git repo and run:
```shell
phpize
./configure --with-php-config=/home/path/to/custom-php/core-autoloading-php/bin/php-config
make -jn
make test TEST_PHP_ARGS="-q -jn"
```

## API

The current API mimics what was proposed in https://wiki.php.net/rfc/autoload_classmap

```php
function autoload_register_class_map(array $classes): void {}

function autoload_register_function_map(array $functions): void {}
```

One difference is that the content of the maps are verified during assignment.

### Evolution

The objective of this extension is to be able to iterate on the API before proposing it (and thus freezing the API)
to be added to PHP Core.
